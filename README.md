## Instructions and solution for Problem 1

Service is deployed on ```https://mars-problem1.herokuapp.com/```. The first and most obvious shortcoming is that the hashes are just stored in node, meaning that the hashes are lost on service restart. In order to bring this microservice into production and allow it to scale better, the hashes would need to be persisted in some form of database, or perhaps cloud storage. I chose to deploy on Heroku primarly due to ease of use; if large user volume was expected, I would consider AWS for better integration with a DynamoDB instance, etc.

# Running instructions (local):

* Have node installed
* Install express with ```npm i```
* Start the application against ```localhost``` with ```npm start```
* Test the hash endpoints with ```curl -X POST -H "Content-Type: application/json" -d '{"message": "foo"}' http://localhost:3000/messages``` and ```curl -X GET -H "Content-Type: application/json" -d '{"message": "foo"}' http://localhost:3000/messages/2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae```
* Equivalent endpoints on the Heroku deployment can be reached at ```curl -X POST -H "Content-Type: application/json" -d '{"message": "foo"}' https://mars-problem1.herokuapp.com/messages``` and ```curl -X GET -H "Content-Type: application/json" -d '{"message": "foo"}' https://mars-problem1.herokuapp.com/messages/2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae```

## Instructions and solution for Problem 2

Big O run time is O(n), as the list is never re-traversed.

# Running instructions:

* Have JDK installed
* Compile the program manually by navigating to ```./src/com``` and running ```javac Solution.java```
* Run the program from the ```./src``` directory with ```java com.Solution <desired list of gifts> <desired maximum gift total>```
    * Example: ```java com.Solution gifts.txt 2100```
* In order to supply a different list of gifts, update one of the existing lists, or supply your own text list in the ```src``` directory and designate in the command line like so:
    * Example: ```java com.Solution yourOwnTextFileList.txt 2100```
