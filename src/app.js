const express = require('express');
const bodyParser = require('body-parser');
const crypto = require('crypto');
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const hashes = [];

app.post('/messages', (req, res) => {
    const hash = crypto.createHash('sha256').update(req.body.message.toString()).digest('hex');

    hashes[hash] = req.body.message.toString();

    res.send({digest: hash});
});

app.get('/messages/:hash', (req, res) => {
    const hash = req.params.hash;
    if(hashes[hash])
    {
        res.send(hashes[hash]);
    }
    else{
        res.status(404);
        res.send();
    }
});

app.listen(process.env.PORT || 3000, () => {});