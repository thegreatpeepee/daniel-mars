package com;

import java.util.*;
import java.io.*;

public class Solution {

    /* List of gift objects read from static file */
    private List<Gift> gifts = new ArrayList<>();

    /*
       Method for finding the two gifts with the sum closets to the given total.
       @param total The total sum to match against.
       @return giftIndicies List of two gift objects with sum closest to total.
     */
    private List<Gift> findGifts(Integer total)
    {
        int sum = 0;
        int listStart = 0;
        int listEnd = gifts.size() - 1;
        List<Gift> giftIndices = new ArrayList<>();
        while(listStart != gifts.size() && listStart != listEnd)
        {
            int indexedSum = gifts.get(listStart).price + gifts.get(listEnd).price;
            // If your current sum is too high, go down to the next most expensive gift
            if(indexedSum > total)
            {
                listEnd--;
            }
            // If your current sum is not higher than one encountered already, go up next cheapest gift
            else {
                if(indexedSum > sum) {
                    giftIndices = Arrays.asList(gifts.get(listStart), gifts.get(listEnd));
                    sum = indexedSum;
                }
                listStart++;
            }
        }
        return giftIndices;
    }

    private void parseGiftList(String fileName)
    {
        try {
            Scanner scanner = new Scanner(new File( fileName));

            while(scanner.hasNextLine()) {

                String line = scanner.nextLine();

                Scanner lineScanner = new Scanner(line).useDelimiter(", ");
                String name = lineScanner.next();
                Integer price = lineScanner.nextInt();

                System.out.println("Adding " + name + " for " + price);
                gifts.add(new Gift(price, name));
            }


        } catch(FileNotFoundException exception) {
            System.out.println("File parsing error: " + exception.getMessage());
        }
    }

    /*
        A poor substitute for unit tests; tests the findGifts method with various totals.
     */
    private void testSuite()
    {
        parseGiftList("gifts.txt");

        System.out.println("Testing with 9000 total.");
        List<Gift> testOutput = findGifts(9000);
        if(testOutput.isEmpty() || testOutput.get(0).price != 2000 || testOutput.get(1).price != 6000)
        {
            throw new AssertionError();
        }

        System.out.println("Testing with 2100 total.");
        testOutput = findGifts(2100);
        if(testOutput.isEmpty() || testOutput.get(0).price != 700 || testOutput.get(1).price != 1400)
        {
            throw new AssertionError();
        }

        System.out.println("Testing with 1500 total.");
        testOutput = findGifts(1500);
        if(testOutput.isEmpty() || testOutput.get(0).price != 500 || testOutput.get(1).price != 1000)
        {
            throw new AssertionError();
        }

        System.out.println("Testing with 100 total.");
        testOutput = findGifts(100);
        if(!testOutput.isEmpty())
        {
            throw new AssertionError();
        }

        gifts = new ArrayList<>();
    }

    /*
        Main method responsible for taking in two CLI parameters, the local file name and the desired total
     */
    public static void main(String... args) {

        Solution solution = new Solution();

        System.out.println("Running verification suite");
        System.out.println();
        solution.testSuite();

        System.out.println();
        System.out.println();
        solution.parseGiftList(args[0]);


        System.out.println("Reading user requested total");
        // Retrieve total from CLI
        Integer total = Integer.parseInt(args[1]);

        List<Gift> output = solution.findGifts(total);

        System.out.println();
        System.out.println();

        if(!output.isEmpty()) {
            System.out.println("Two gifts closest to total: ");
            System.out.println(output);
        }
        else {
            System.out.println("No matching gifts found.");
        }
    }


    private static class Gift {
        public int price;
        public String name;

        public Gift(int price, String name)
        {
            this.price = price;
            this.name = name;
        }

        public String toString()
        {
            return name + " " + price;
        }
    }
}
